<?php

namespace App\Tools;

use Carbon\Carbon;

class FormatsTool {

    public function prepareToSave($campo, $valor)
    {
        $res = '';
        switch ($campo) {
            case 'date':
                $res = Carbon::createFromDate($valor)->format(config('tools.formats.date.database'));
                break;
            case 'decimal':
                $res = str_replace(',', '.', str_replace('.', '', $valor));
                break;
        }
        return $res;
    }
    
}
