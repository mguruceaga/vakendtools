<?php

namespace App\Managers;

use App\Models\Abogado;
use App\Models\Persona;
use App\Models\Ticket;
use App\Models\TicketEstado;
use App\Models\TicketEstadoRelacion;
use App\Models\TicketUsuario;
use App\Models\Reparticion;
use App\Models\Rol;
use App\Models\User;
use App\Tools\FormatsTool;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TicketManager extends BaseManager {

    public function __construct($entity = null) {
        $this->entityClassName = '\App\Models\Ticket';
        parent::__construct($entity);
    }

    public function newItem() {
        $ticket = new Ticket();
        return $ticket;
    }

    public function getItem($ticketId) {
        $ticket = Ticket::find($ticketId);
        return $ticket;
    }

    public function saveItem(Ticket $ticket) {

        $ticket->user_id = Auth::user()->id;
        $ticket->save();
        $ticketEstadoRelacion = TicketEstadoRelacion::create([
            'ticket_id' => $ticket->id,
            'estado_id' => $ticket->estado_id,
            'user_id' => Auth::user()->id,
        ]);
        return $ticket;

    }

    public function changeState(Ticket $ticket, $estadoSlug) {
        $ticketEstado = TicketEstado::where('slug', $estadoSlug)
            ->first();
        $rol = Rol::where('name', config('tools.constants.roles.receptor'))
            ->first();
        if ($estadoSlug == 'en_revision') {
            /**
             * Busco a quien le asigné la última ticket
             */
            $ultimoAsigado = 0;
            $ticketUsuario = TicketUsuario::orderBy('created_at', 'DESC')
                ->first();
            if (!is_null($ticketUsuario))
                $ultimoAsigado = $ticketUsuario->user_id;
            $empleado = DB::table('users')
                ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                ->where('users.recibe_tickets', true)
                ->where('users.id', '>', $ultimoAsigado)
                ->where('model_has_roles.role_id', $rol->id)
                ->orderBy('users.id')
                ->first();
            if (is_null($empleado))
                $empleado = DB::table('users')
                    ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                    ->where('users.recibe_tickets', true)
                    ->where('model_has_roles.role_id', $rol->id)
                    ->orderBy('users.id')
                    ->first();
            $ticketUsuario = TicketUsuario::create([
                'ticket_id' => $ticket->id,
                'user_id' => $empleado->id,
            ]);
            $ticket->save();
        }
        $ticketEstadoRelacion = TicketEstadoRelacion::create([
            'ticket_id' => $ticket->id,
            'estado_id' => $ticketEstado->id,
            'user_id' => Auth::user()->id,
        ]);
        return $ticketEstadoRelacion;
    }

    public function getFilterItems($asunto, $ordenarPor, $sentido) {
        $roles = auth()->user()->getRoleNames();
        $listado = new Collection();
        switch ($roles[0]) {
            case config('tools.constants.roles.administrador'):
                $listado = Ticket::search('asunto', $asunto)
                    ->orderBy($ordenarPor, $sentido)
                    ->paginate(config('tools.constants.custom.items_per_page'));
                break;
            case config('tools.constants.roles.receptor'):
                $listado = Ticket::search('asunto', $asunto)
                    ->orderBy($ordenarPor, $sentido)
                    ->paginate(config('tools.constants.custom.items_per_page'));
                break;
            case config('tools.constants.roles.cliente'):
                $listado = Ticket::search('asunto', $asunto)
                    ->where('user_id', Auth::user()->id)
                    ->orderBy($ordenarPor, $sentido)
                    ->paginate(config('tools.constants.custom.items_per_page'));
                break;
        }
        return $listado;
    }

    public function getItemStates(Ticket $ticket) {
        return DB::table('ticket_estado')
            ->join('ticket_estados', 'ticket_estados.id', 'ticket_estado.estado_id')
            ->join('users', 'users.id', 'ticket_estado.user_id')
            ->where('ticket_estado.ticket_id', $ticket->id)
            ->select('ticket_estados.nombre', 'ticket_estados.color', 'ticket_estados.slug',
                DB::raw('DATE_FORMAT(ticket_estado.created_at, "%d-%m-%Y %H:%k:%S") as fecha'),
                'users.name')
            ->get();
    }

}
