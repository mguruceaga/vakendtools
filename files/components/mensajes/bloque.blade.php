@props([
    'ticket' => null,
    'mensaje' => null,
])

<div class="flex-col w-full py-4 bg-white border-bottom border-gray-200 sm:px-4 sm:py-4 md:px-4 sm:shadow-sm">

    <livewire:mensaje-show :mensaje="$mensaje" :key="'mensaje_show_'.$mensaje->id" />

</div>