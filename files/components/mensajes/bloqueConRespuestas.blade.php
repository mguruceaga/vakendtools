@props([
    'ticket' => null,
    'mensaje' => null,
])

<div class="flex-col w-full py-4 bg-white border border-gray-200 sm:px-4 sm:py-4 md:px-4 sm:rounded-lg sm:shadow-sm">

    <livewire:mensaje-show :mensaje="$mensaje" :key="'mensaje_show_'.$mensaje->id" />

    <hr class="my-2 ml-16 border-gray-200">
    @if ($mensaje->respuestas()->count() > 0)
        @foreach($mensaje->respuestas() as $respuesta)
            <livewire:mensaje-show :mensaje="$respuesta" :key="'mensaje_show_'.$respuesta->id" />
            <hr class="my-2 ml-16 border-gray-200">
        @endforeach
    @endif
</div>