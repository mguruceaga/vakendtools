<form id="formTicket" wire:submit.prevent="save" autocomplete="off">

    <div class="shadow overflow-hidden sm:rounded-md z-0">
        <div class="px-4 py-6 bg-white sm:p-6">

            <div class="px-4 sm:px-0">
                <h4 class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
                    Datos del ticket
                </h4>
            </div>

            <x-custom.hr />

            <div class="grid grid-cols-6 gap-6 mb-4">
                <div class="col-span-6 sm:col-span-4">
                    <x-custom.forms.input.group label="Proyecto" for="proyecto_id" :error="$errors->first('elem.proyecto_id')">
                        <x-custom.forms.input.select wire:model.lazy="elem.proyecto_id" name="proyecto_id" id="proyecto_id"
                                                     :options="$proyectos"
                                                     :error="$errors->first('elem.proyecto_id')"/>
                    </x-custom.forms.input.group>
                </div>
            </div>

            <div class="grid grid-cols-6 gap-6 mb-4">
                <div class="col-span-6 sm:col-span-2">
                    <x-custom.forms.input.group label="Tipo" for="ticket_tipo_id" :error="$errors->first('elem.ticket_tipo_id')">
                        <x-custom.forms.input.select wire:model.lazy="elem.ticket_tipo_id" name="ticket_tipo_id" id="ticket_tipo_id"
                                                     :options="$ticketTipos"
                                                     :error="$errors->first('elem.ticket_tipo_id')"/>
                    </x-custom.forms.input.group>
                </div>
                <div class="col-span-6 sm:col-span-2">
                    <x-custom.forms.input.group label="Nivel de Importancia" for="nivel_importancia_id" :error="$errors->first('elem.nivel_importancia_id')">
                        <x-custom.forms.input.select wire:model.lazy="elem.nivel_importancia_id" name="nivel_importancia_id" id="nivel_importancia_id"
                                                     :options="$nivelesImportancia"
                                                     :error="$errors->first('elem.nivel_importancia_id')"/>
                    </x-custom.forms.input.group>
                </div>
                <div class="col-span-6 sm:col-span-2">
                    <x-custom.forms.input.group label="Estado" for="estado_id" :error="$errors->first('elem.estado_id')">
                        <x-custom.forms.input.select wire:model.lazy="elem.estado_id" name="estado_id" id="estado_id"
                                                     :options="$ticketEstados"
                                                     :error="$errors->first('elem.estado_id')"/>
                    </x-custom.forms.input.group>
                </div>
            </div>

            <div class="grid grid-cols-6 gap-6 mb-4">
                <div class="col-span-6 sm:col-span-6">
                    <x-custom.forms.input.group label="Asunto" for="asunto" :error="$errors->first('elem.asunto')">
                        <x-custom.forms.input.text wire:model.lazy="elem.asunto" name="asunto" id="asunto"
                                                   :error="$errors->first('elem.asunto')"/>
                    </x-custom.forms.input.group>
                </div>
            </div>

            <div class="grid grid-cols-6 gap-6 mb-4">
                <div class="col-span-6 sm:col-span-6">
                    <x-custom.forms.input.group-rich-text label="Descripción" for="descripcion" :error="$errors->first('elem.descripcion')">
                        <x-custom.forms.input.rich-text
                                name="descripcion"
                                wire:model.lazy="elem.descripcion"
                                id="descripcion"
                                :key="'descripcion'"
                                :initial-value="$elem->descripcion"
                                :error="$errors->first('elem.descripcion')"
                                @trix-attachment-add="console.log($event.attachment)"
                        />
                    </x-custom.forms.input.group-rich-text>
                </div>

            </div>

        </div>
    </div>

    <div class="shadow overflow-hidden sm:rounded-md z-30">
        <div class="px-4 py-6 bg-white sm:p-6">
            <div class="text-right sm:px-6">
                <x-custom.forms.buttons.a href="{{ route('tickets') }}" color="secondary">
                    Cancelar
                </x-custom.forms.buttons.a>
                <x-custom.forms.buttons.primary color="indigo">
                    Guardar
                </x-custom.forms.buttons.primary>
            </div>
        </div>
    </div>

</form>