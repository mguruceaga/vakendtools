@props([
    'color' => 'indigo',
    'icon' => '',
])

<button {{ $attributes }}
        type="button"
        class="inline-flex items-center px-4 py-2 border border-transparent
        rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-{{ $color }}-700
        focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-{{ $color }}-500">
    <x-custom.icon.small-chat-alt-2 class="text-white bg-white" />
    {{ $slot }}
</button>

<button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
    <svg class="-ml-1 mr-2 h-10 w-10" x-description="Heroicon name: check" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
        <path d="M2 5a2 2 0 012-2h7a2 2 0 012 2v4a2 2 0 01-2 2H9l-3 3v-3H4a2 2 0 01-2-2V5z" />
        <path d="M15 7v2a4 4 0 01-4 4H9.828l-1.766 1.767c.28.149.599.233.938.233h2l3 3v-3h2a2 2 0 002-2V9a2 2 0 00-2-2h-1z" />
    </svg>
    <span class="float-left">10</span>
</button>