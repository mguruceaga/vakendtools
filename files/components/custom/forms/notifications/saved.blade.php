<span
    x-data="{ open: false }"
    x-init="
                @this.on('notify-saved', () => {
                    setTimeout(() => { open = false }, 2500);
                    open = true;
                })
            "
    x-show.transition.out.duration.1000ms="open"
    x-ref="this"
    style="display: none;"
    class="text-green-500"
>¡El elemento se ha guardado con éxito!</span>