    @props([
        'label',
        'for',
        'currency' => false,
        'error' => false,
    ])

    <label class="block font-medium text-sm text-gray-700" for="{{ $for }}">
        {{ $label }}
    </label>

    {{ $slot }}

    @if ($error)
        <p class="text-sm text-red-600 mt-2">{{ $error }}</p>
    @endif

    <script>
        function uploadTrixImage(attachment) {
            console.log(attachment);
            @this.upload(
                'files',
                attachment.file,
                function(uploadUrl) {
                    console.log(uploadUrl);
                    const eventName = 'myapp:trix-upload-completed:${btoa(uploadUrl)}';
                    const listener = function(event) {
                        if (!attachment.file) {
                            var href = attachment.getAttribute("href")
                            var url = attachment.getAttribute("url")
                            if (!href && url) {
                                attachment.setAttributes({href: url})
                            }
                        } else {
                            attachment.setAttributes(event.detail);
                        }
                        window.removeEventListener(eventName, listener);
                    };
                    window.addEventListener(eventName, listener);
                @this.call('completedUpload', uploadUrl, eventName);
                },
                function() {},
                function(event) {
                    attachment.setUploadProgress(event.detail.progress);
                },
            );
        }
    </script>